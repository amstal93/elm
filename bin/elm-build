#!/bin/sh

set -e

build() {
  readonly src="${1}";
  readonly min="${2}";
  readonly directory=$(dirname "${min}");
  readonly elm_output="$(mktemp).js";

  # Create directory if it doesn't exist
  if [ ! -d "${directory}" ]; then
    mkdir -p "${directory}";
  fi

  elm make --optimize --output="${elm_output}" "${src}"
  uglifyjs "${elm_output}" \
    --compress='pure_funcs="F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9",pure_getters,keep_fargs=false,unsafe_comps,unsafe' \
    | uglifyjs --mangle --output="${min}"

  cat <<EOF
  Compiled size:$(wc -c "${elm_output}") bytes  ("${elm_output}")
  Minified size:$(wc -c "${min}") bytes  ("${min}")
  Gzipped size: $(gzip -c "${min}"| wc -c) bytes
EOF
}

show_help(){
  cat <<EOF 
Usage: elm-build -s 'src/Main.elm' -o 'dist/app.js'
Build and optimize your Elm project.

  -s  main Elm module, e.g. 'src/Main.elm'
  -o  JavaScript file, e.g. 'dist/app.js'
EOF

exit 1;
}

main () {
  while getopts ":s:o:" opt; do
    case $opt in
      s) src="$OPTARG"
      ;;
      o) out="$OPTARG"
      ;;
      \?) show_help; 
      ;;
    esac
  done

  build "${src}" "${out}"
}

main "${@}"
