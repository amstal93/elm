FROM node:lts-slim

LABEL name="elm"
LABEL version="2.2.0"
LABEL maintainer="Luís Couto <couto@15minuteslate.net>"

ARG ELM_VERSION=0.19

# This process is a bit different.
# We are installing each "global" package as a local package inside /opt and
# then we symlink it to /usr/local/bin manually
#
# The reason why we do this, is to have an easy access to the `npm audit`
# command, which is a really important command that notify us whenever we
# install a dependency with a known vulnerability.
#
# This check is not done for real global packages
RUN cd /opt \
  && npm init --yes \
  && npm install --production \
    "elm@${ELM_VERSION}" \
    elm-test \
    elm-verify-examples \
    elm-format \
    elm-oracle \
    create-elm-app \
    uglify-js \
  && npm cache clean --force

RUN ln -s /opt/node_modules/.bin/* /usr/local/bin/

# Add binary to build optimized elm files
COPY ./bin/elm-build /usr/local/bin/elm-build
RUN chmod +x /usr/local/bin/elm-build

EXPOSE 8000

ENTRYPOINT ["elm"]
CMD ["--help"]
